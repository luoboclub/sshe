<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript">
	$(function() {
		$('#user_login_LoginForm').form({
			url : '${pageContext.request.contextPath}/userAction!login.action',
			success : function(data) {
				var obj = jQuery.parseJSON(data);
				if (obj.success) {
					$('#user_login_loginDialog').dialog('close');
				}
				$.messager.show({
					title : '提示',
					msg : obj.msg
				});
			}
		});

		$("#user_login_LoginForm input").bind('keyup', function(event) {
			if (event.keyCode == '13') {
				$('#user_login_LoginForm').submit();
			}
		});

		$("#user_login_LoginForm input[name=name]").focus();
	});
</script>
<!-- 登录 -->
<div id="user_login_loginDialog" style="width:250px" class="easyui-dialog" data-options="title:'登录',modal:true,closable:false,buttons:[{
				text:'注册',
				iconCls:'icon-edit',
				handler:function(){
					$('#user_reg_regDialog').dialog('open');
				}
			},{
				text:'登录',
				iconCls:'icon-help',
				handler:function(){
					$('#user_login_LoginForm').submit();
				}
			}]">
	<form id="user_login_LoginForm" method="post">
		<table>
			<tr>
				<th>登录名</th>
				<td><input name="name" class="easyui-validatebox" data-options="required:true,missingMessage:'登录名不能为空'" /></td>
			</tr>
			<tr>
				<th>密码</th>
				<td><input name="pwd" type="password" class="easyui-validatebox" data-options="required:true,missingMessage:'密码不能为空'" /></td>
			</tr>
		</table>
	</form>
</div>