<%@ page language="java" pageEncoding="UTF-8"%>


<!DOCTYPE HTML>
<html>
<head>

<title>SSH DEMO</title>
<script type="text/javascript" src="jslib/jquery-easyui-1.4.5/jquery.min.js"></script>
<script type="text/javascript" src="jslib/jquery-easyui-1.4.5/jquery.easyui.min.js"></script>
<script type="text/javascript" src="jslib/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="jslib/JsUtil.js"></script>
<link rel="stylesheet" href="jslib/jquery-easyui-1.4.5/themes/default/easyui.css">
<link rel="stylesheet" href="jslib/jquery-easyui-1.4.5/themes/icon.css">

</head>

<body class="easyui-layout">
	<div data-options="region:'north'" style="height:60px;"></div>
	<div data-options="region:'south'" style="height:20px;"></div>
	<div data-options="region:'west'" style="width:200px;">
		<jsp:include page="layout/west.jsp"></jsp:include>
	</div>

	<div data-options="region:'east',title:'east',split:true" style="width:200px;"></div>
	<div data-options="region:'center',title:'欢迎使用'">
		<jsp:include page="layout/center.jsp"></jsp:include>
	</div>
	<jsp:include page="user/login.jsp"></jsp:include>
	<jsp:include page="user/reg.jsp"></jsp:include>

</body>
</html>
