<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript">
	$('#jsp_yhgl_datagrid').datagrid({
		url : '${pageContext.request.contextPath}/userAction!datagrid.action',
		fit : true,
		fitColumns : true,
		border : false,
		pagination : true,
		sortName : 'name',
		sortOrder : 'asc',
		checkOnSelect : false,
		selectOnCheck : false,
		idField : 'id',
		frozenColumns : [ [ {
			field : 'id',
			title : '编号',
			width : 150,
			checkbox : true,
		}, ] ],
		columns : [ [ {
			field : 'name',
			title : '登录名称',
			width : 150,
			sortable : true,
		}, {
			field : 'pwd',
			title : '密码',
			width : 150,
			formatter : function(value, row, index) {
				return '******';
			}
		}, {
			field : 'createdatetime',
			title : '创建时间',
			width : 150,
			sortable : true,
		}, {
			field : 'modifydatetime',
			title : '最后修改时间',
			width : 150,
			sortable : true,
		} ] ],
		toolbar : [ {
			text : '增加',
			iconCls : 'icon-add',
			handler : function() {
				append();
			},
		}, '-', {
			text : '删除',
			iconCls : 'icon-remove',
			handler : function() {
				remove();
			}
		}, '-', {
			text : '修改',
			iconCls : 'icon-edit',
			handler : function() {
				editFun();
			}
		}, '-' ]
	});

	function editFun() {
		var rows = $('#jsp_yhgl_datagrid').datagrid('getChecked');
		if (rows.length == 1) {
			var d = $('<div/>').dialog({
				title : '修改',
				width : 500,
				height : 200,
				href : '${pageContext.request.contextPath}/jsp/yhglEdit.jsp',
				modal : true,
				buttons : [ {
					text : '编辑',
					handler : function() {
						$('#jsp_yhglEdit_editForm').form('submit', {
							url : '${pageContext.request.contextPath}/userAction!edit.action',
							success : function(data) {
								var obj = jQuery.parseJSON(data);
								if (obj.success) {
									d.dialog('close');
									$('#jsp_yhgl_datagrid').datagrid('updateRow', {
										index : $('#jsp_yhgl_datagrid').datagrid('getRowIndex', rows[0].id),
										row : obj.obj
									});
								}
								$.messager.show({
									title : '提示',
									msg : obj.msg
								});
							}
						});
					}
				} ],
				onClose : function() {
					$(this).dialog('destroy');
				},
				onLoad : function() {
					$('#jsp_yhglEdit_editForm').form('load', rows[0]);
				},
			});
		} else {
			$.messager.alert('提示', '请选择一条记录进行编辑!');
		}

	}

	function searchFun() {
		$('#jsp_yhgl_datagrid').datagrid('load', serializeObject($('#jsp_yhgl_searchForm')));
	}

	function clearFun() {
		$('#jsp_yhgl_layout input[name=name]').val('');
		$('#jsp_yhgl_datagrid').datagrid('load', {});
	}

	function append() {
		$('#jsp_yhgl_addForm input').val('');
		$('#jsp_yhgl_addDialog').dialog('open');
	}

	function remove() {
		var rows = $('#jsp_yhgl_datagrid').datagrid('getChecked');
		var ids = [];
		if (rows.length > 0) {
			$.messager.confirm('确认', '您是否要删除当前选中的项目？', function(r) {
				if (r) {
					for (var i = 0; i < rows.length; i++) {
						ids.push(rows[i].id);
					}
					$.ajax({
						url : '${pageContext.request.contextPath}/userAction!remove.action',
						data : {
							ids : ids.join(',')
						},
						dataType : 'json',
						success : function(r) {
							$('#jsp_yhgl_datagrid').datagrid('load');
							$('#jsp_yhgl_datagrid').datagrid('unselectAll');
							$.messager.show({
								title : '提示',
								msg : r.msg
							});
						}
					});
				}

			});

		} else {
			$.messager.show({
				title : '提示',
				msg : '勾选要删除的记录'
			});
		}
	}
</script>
<div id="jsp_yhgl_layout" class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'north',title:'查询条件',border:false" style="height:100px">
		<form id="jsp_yhgl_searchForm">
			检索用户名称(可模糊查询)：<input name="name" /> <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="searchFun();">查询</a> <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true" onclick="clearFun();">清空</a>
		</form>
	</div>
	<div data-options="region:'center'">
		<table id="jsp_yhgl_datagrid"></table>
	</div>
</div>

<div id="jsp_yhgl_addDialog" data-options="title:'添加用户',closed:true,modal:true,buttons : [ {
			text : '增加',
			iconCls : 'icon-add',
			handler : function() {
					$('#jsp_yhgl_addForm').form('submit',{
						url:'${pageContext.request.contextPath}/userAction!add.action',
						success:function(data){
							var obj = jQuery.parseJSON(data);
							if (obj.success) {
								$('#jsp_yhgl_addDialog').dialog('close');
								//$('#jsp_yhgl_datagrid').datagrid('appendRow',obj.obj);
								$('#jsp_yhgl_datagrid').datagrid('insertRow',{
										index: 0,	
										row: obj.obj
								});
							}
							$.messager.show({
								title : '提示',
								msg : obj.msg
							});
					}
				});
			},
		}]," class="easyui-dialog" style="width:500px;height:200px;">
	<form id="jsp_yhgl_addForm" method="post">
		<table style="align:center">
			<tr>
				<th>编号</th>
				<td><input name="id" readonly="readonly"></td>

				<th>登录名称</th>
				<td><input name="name" class="easyui-validatebox" data-options="required:true"></td>
			</tr>
			<tr>
				<th>密码</th>
				<td><input name="pwd" type="password" class="easyui-validatebox" data-options="required:true"></td>

				<th>创建时间</th>
				<td><input name="createdatetime" readonly="readonly"></td>
			</tr>
			<tr>
				<th>最后修改时间</th>
				<td><input name="modifydatetime" readonly="readonly"></td>
			</tr>
		</table>
	</form>
</div>
