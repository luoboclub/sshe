package cn.it.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface BaseDao<T> {
	public Serializable save(T entity);
	
	public T get(String hql);
	
	public T get(Class<T> c,Serializable id);
	
	public T get(String hql,Map<String, Object> params);

	public void delete(T entity);
	
	public void update(T entity);
	
	public void saveOrUpdate(T entity);
	
	public List<T> find(String hql);
	
	public List<T> find(String hql,int page,int rows);
	
	public List<T> find(String hql,Map<String, Object> params);
	
	public List<T> find(String hql,Map<String, Object> params,int page,int rows);
	
	public Long count(String hql);
	
	public Long count(String hql,Map<String, Object> params);
	
	public int executeHql(String hql);
}
