package cn.it.dao.impl;

import org.springframework.stereotype.Repository;

import cn.it.dao.UserDao;
import cn.it.model.Tuser;

@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<Tuser> implements UserDao {

}
