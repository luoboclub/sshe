package cn.it.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.it.dao.BaseDao;

@Repository("baseDao")
public class BaseDaoImpl<T> implements BaseDao<T> {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Serializable save(T entity) {
		return this.sessionFactory.getCurrentSession().save(entity);
	}

	@Override
	public T get(String hql, Map<String, Object> params) {
		Query qr = this.sessionFactory.getCurrentSession().createQuery(hql);
		if (params != null && !params.isEmpty()) {
			for (String key : params.keySet()) {
				qr.setParameter(key, params.get(key));
			}
		}

		List<T> list = qr.list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public T get(Class<T> c,Serializable id) {
		// TODO Auto-generated method stub
		return  (T) this.getSessionFactory().getCurrentSession().get(c, id);
	}
	
	@Override
	public T get(String hql) {
		Query qr = this.sessionFactory.getCurrentSession().createQuery(hql);
		List<T> list = qr.list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public void delete(T entity) {
		this.sessionFactory.getCurrentSession().delete(entity);

	}

	@Override
	public void update(T entity) {
		this.sessionFactory.getCurrentSession().update(entity);

	}

	@Override
	public void saveOrUpdate(T entity) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(entity);
	}

	@Override
	public List<T> find(String hql) {
		Query qr = this.sessionFactory.getCurrentSession().createQuery(hql);
		return qr.list();
	}

	@Override
	public List<T> find(String hql, Map<String, Object> params) {
		Query qr = this.sessionFactory.getCurrentSession().createQuery(hql);
		if (params != null && !params.isEmpty()) {
			for (String key : params.keySet()) {
				qr.setParameter(key, params.get(key));
			}
		}
		return qr.list();
	}

	@Override
	public List<T> find(String hql, Map<String, Object> params, int page,
			int rows) {
		Query qr = this.sessionFactory.getCurrentSession().createQuery(hql);
		if (params != null && !params.isEmpty()) {
			for (String key : params.keySet()) {
				qr.setParameter(key, params.get(key));
			}
		}
		return qr.setFirstResult((page - 1) * rows).setMaxResults(rows).list();
	}

	@Override
	public List<T> find(String hql, int page, int rows) {
		Query qr = this.sessionFactory.getCurrentSession().createQuery(hql);
		return qr.setFirstResult((page - 1) * rows).setMaxResults(rows).list();

	}

	@Override
	public Long count(String hql) {
		Query qr = this.sessionFactory.getCurrentSession().createQuery(hql);
		return (Long) qr.uniqueResult();
	}

	@Override
	public Long count(String hql, Map<String, Object> params) {
		Query qr = this.sessionFactory.getCurrentSession().createQuery(hql);
		if (params != null && !params.isEmpty()) {
			for (String key : params.keySet()) {
				qr.setParameter(key, params.get(key));
			}
		}
		return (Long) qr.uniqueResult();
	}

	@Override
	public int executeHql(String hql) {
		Query q = this.sessionFactory.getCurrentSession().createQuery(hql);
		return q.executeUpdate();
	}

	
}
