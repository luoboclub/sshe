package cn.it.service;


import cn.it.pageModel.DataGrid;
import cn.it.pageModel.User;



public interface UserService {
	
public User save(User user);

public User login(User user);

public DataGrid datagrid(User user);

public void remove(String ids);

public User edit(User user);


}
