package cn.it.service;

import java.util.List;

import cn.it.pageModel.Menu;


public interface MenuService {

	public List<Menu> getTree(String id);
}
