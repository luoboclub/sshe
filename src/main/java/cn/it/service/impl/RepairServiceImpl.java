package cn.it.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.it.dao.MenuDao;
import cn.it.dao.UserDao;
import cn.it.model.Tmenu;
import cn.it.model.Tuser;
import cn.it.service.RepairService;
import cn.it.util.Encrypt;

@Service("repairService")
public class RepairServiceImpl implements RepairService {

	private MenuDao menuDao;
	private UserDao userDao;

	public UserDao getUserDao() {
		return userDao;
	}

	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public MenuDao getMenuDao() {
		return menuDao;
	}

	@Autowired
	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
	}

	@Override
	public void repair() {
		repairUser();
		repairMenu();
	}

	private void repairUser() {
		Map<String,Object> m=new HashMap<String,Object>();
		m.put("name", "admin");
		Tuser t = userDao.get("from Tuser t where t.name=:name and t.id!='0'",m);
		if(t!=null){
			t.setName(UUID.randomUUID().toString());
		}
		Tuser admin=new Tuser();
		admin.setId("0");
		admin.setName("admin");
		admin.setPwd(Encrypt.e("admin"));
		admin.setModifydatetime(new Date());
		userDao.saveOrUpdate(admin);
	}

	private void repairMenu() {
		Tmenu root = new Tmenu();
		root.setId("0");
		root.setText("首页");
		root.setUrl("");
		menuDao.saveOrUpdate(root);
		
		Tmenu xtgl=new Tmenu();
		xtgl.setId("xtgl");
		xtgl.setTmenu(root);
		xtgl.setText("系统管理");
		xtgl.setUrl("");
		menuDao.saveOrUpdate(xtgl);
		
		Tmenu yhgl=new Tmenu();
		yhgl.setId("yhgl");
		yhgl.setTmenu(xtgl);
		yhgl.setText("用户管理");
		yhgl.setUrl("/jsp/yhgl.jsp");
		menuDao.saveOrUpdate(yhgl);
		
		Tmenu jsgl=new Tmenu();
		jsgl.setId("jsgl");
		jsgl.setTmenu(xtgl);
		jsgl.setText("角色管理");
		jsgl.setUrl("");
		menuDao.saveOrUpdate(jsgl);
		
		Tmenu qxgl=new Tmenu();
		qxgl.setId("qxgl");
		qxgl.setTmenu(xtgl);
		qxgl.setText("权限管理");
		qxgl.setUrl("");
		menuDao.saveOrUpdate(qxgl);
		
		Tmenu cdgl=new Tmenu();
		cdgl.setId("cdgl");
		cdgl.setTmenu(xtgl);
		cdgl.setText("菜单管理");
		cdgl.setUrl("");
		menuDao.saveOrUpdate(cdgl);
		
		Tmenu buggl=new Tmenu();
		buggl.setId("buggl");
		buggl.setTmenu(xtgl);
		buggl.setText("BUG管理");
		buggl.setUrl("");
		menuDao.saveOrUpdate(buggl);
		
	}

	
}
