package cn.it.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.it.dao.UserDao;
import cn.it.model.Tuser;
import cn.it.pageModel.DataGrid;
import cn.it.pageModel.User;
import cn.it.service.UserService;
import cn.it.util.Encrypt;

@Service("userService")
public class UserServiceImpl implements UserService {

	private UserDao userDao;

	public UserDao getUserDao() {
		return userDao;
	}

	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public User save(User user) {
		Tuser t = new Tuser();
		BeanUtils.copyProperties(user, t, new String[] { "pwd" });
		t.setId(UUID.randomUUID().toString());
		t.setCreatedatetime(new Date());
		t.setPwd(Encrypt.e(user.getPwd()));
		userDao.save(t);
		BeanUtils.copyProperties(t, user);
		return user;
	}

	@Override
	public User login(User user) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pwd", Encrypt.e(user.getPwd()));
		params.put("name", user.getName());
		Tuser u = userDao.get("from Tuser t where t.name=:name and t.pwd=:pwd", params);
		if (u != null) {
			return user;
		} else {
			return null;
		}

	}

	@Override
	public DataGrid datagrid(User user) {
		DataGrid dg = new DataGrid();
		String hql = "from Tuser t";

		Map<String, Object> paramas = new HashMap<String, Object>();
		hql = addWhere(user, hql, paramas);

		String counthql = "select count(*)" + hql;
		hql = addOrder(user, hql);

		List<Tuser> tuserlist = userDao.find(hql, paramas, user.getPage(), user.getRows());
		List<User> userList = new ArrayList<User>();
		changeModel(tuserlist, userList);
		dg.setTotal(userDao.count(counthql, paramas));
		dg.setRows(userList);
		return dg;
	}

	private void changeModel(List<Tuser> tuserlist, List<User> userList) {
		if (tuserlist != null && tuserlist.size() > 0) {
			for (Tuser tuser : tuserlist) {
				User u = new User();
				BeanUtils.copyProperties(tuser, u);
				userList.add(u);
			}
		}
	}

	private String addOrder(User user, String hql) {
		if (user.getSort() != null) {
			hql += " order by " + user.getSort() + " " + user.getOrder();
		}
		return hql;
	}

	private String addWhere(User user, String hql, Map<String, Object> paramas) {
		if (user.getName() != null && !("").equals(user.getName().trim())) {
			hql += " where t.name like :name";
			paramas.put("name", "%%" + user.getName().trim() + "%%");
		}
		return hql;
	}

	@Override
	public void remove(String ids) {
		String[] nids = ids.split(",");
		String hql = "delete Tuser t where t.id in(";
		for (int i = 0; i < nids.length; i++) {
			if (i > 0) {
				hql += ",";
			}
			hql += "'" + nids[i] + "'";
		}
		hql += ")";
		userDao.executeHql(hql);
	}

	@Override
	public User edit(User user) {
		Tuser tuser = userDao.get(Tuser.class, user.getId());
		BeanUtils.copyProperties(user, tuser, new String[] { "id", "pwd" });
		return user;
	}
}
