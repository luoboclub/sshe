package cn.it.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.it.dao.MenuDao;
import cn.it.model.Tmenu;
import cn.it.pageModel.Menu;
import cn.it.service.MenuService;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

	private MenuDao menuDao;

	public MenuDao getMenuDao() {
		return menuDao;
	}

	@Autowired
	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
	}

	@Override
	public List<Menu> getTree(String id) {
		List<Menu> val = new ArrayList<Menu>();
		String hql=null;
		Map<String,Object> params=new HashMap<String,Object>();
		if(id==null||("").equals(id)){
			//查询所有跟节点
			hql="from Tmenu t where t.tmenu=null";
		}else{
			//异步加载当前id下的子节点
			hql="from Tmenu t where t.tmenu.id=:id";
			params.put("id", id);
		}
		List<Tmenu> list = menuDao.find(hql,params);
		if (list != null && list.size() > 0) {
			for (Tmenu tmenu : list) {
				Menu menu = new Menu();
				BeanUtils.copyProperties(tmenu, menu);
				Map<String, Object> attributes=new HashMap<String, Object>();
				attributes.put("url", tmenu.getUrl());
				Set<Tmenu> set=tmenu.getTmenus();
				if (set != null && !set.isEmpty()) {
					menu.setState("closed");//节点以文件夹的形式体现
				} else {
					menu.setState("open");//节点以文件的形式体现
				}
				menu.setAttributes(attributes);
				val.add(menu);
			}
		}
		return val;
	}

}
