package cn.it.action;



import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;

import cn.it.pageModel.Menu;
import cn.it.service.MenuService;

import com.opensymphony.xwork2.ModelDriven;

@Namespace("/")
@Action(value = "menuAction")
public class MenuAction extends BaseAction implements ModelDriven<Menu> {
	private Menu menu = new Menu();

	private MenuService menuService;

	public MenuService getMenuService() {
		return menuService;
	}

	@Autowired
	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	@Override
	public Menu getModel() {
		// TODO Auto-generated method stub
		return menu;
	}

	public void getTree() {
		super.writeJson(menuService.getTree(menu.getId()));
	}
}
