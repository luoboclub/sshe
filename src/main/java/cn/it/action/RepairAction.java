package cn.it.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;

import cn.it.service.MenuService;
import cn.it.service.RepairService;
import cn.it.service.UserService;

@Namespace("/")
@Action(value = "repairAction")
public class RepairAction extends BaseAction {
	private RepairService repairService;
	private UserService userService;
	private MenuService menuService;

	public MenuService getMenuService() {
		return menuService;
	}

	@Autowired
	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	public UserService getUserService() {
		return userService;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public RepairService getRepairService() {
		return repairService;
	}

	@Autowired
	public void setRepairService(RepairService repairService) {
		this.repairService = repairService;
	}

	public void init() {
		repairService.repair();
	}
}
